from model import Board


def show_board(board):
    for i in range(len(board[0])):
        for j in range(len(board)):
            if board[j][i] == 1:
                print("x", end=" | ")
            elif board[j][i] == -1:
                print("o", end=" | ")
            else:
                print(" ", end=" | ")
        print("\n___________________________\n")


board = Board()

while board.winner == 0:
    show_board(board)
    c = int(input())
    board.drop_in(c)
print("winer:", board.winner)
