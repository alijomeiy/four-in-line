class ListPlayer(list):
    def __init__(self, pointer=0, max_len=-1, min_len=-1, default_value=None):
        super(ListPlayer, self).__init__()
        self._pointer = pointer
        self._max_len = max_len if max_len >= 0 else -1
        self._min_len = min_len if min_len >= 0 else 0
        if default_value != None:
            self.extend([default_value] * self._min_len)
        pass

    @property
    def current(self):
        if len(self) <= 0:
            return
        return self[self._pointer]

    @property
    def pointer(self):
        return self._pointer

    def next(self):
        if self.is_nextable():
            self._pointer += 1
        return self.current

    def is_nextable(self):
        if self._pointer + 1 < len(self):
            return True
        return False

    def previous(self):
        if self._pointer - 1 >= 0:
            self._pointer -= 1
        return self.current

    def append(self, __object) -> None:
        if self._max_len > 0 and len(self) + 1 > self._max_len:
            return
        return super().append(__object)

    def __str__(self) -> str:
        return super().__str__()


class Board:
    def __init__(self) -> None:
        self._board = []
        for i in range(7):
            self._board.append(ListPlayer(
                max_len=6, min_len=6, default_value=0))
        self._turn = 1
        self._winner = 0
        pass

    @property
    def winner(self):
        return self._winner

    def drop_in(self, column_index):
        current_column = self._board[column_index]
        if current_column.is_nextable():
            p = current_column.pointer
            current_column[p] = self._turn
            current_column.next()
            self._turn *= -1
            self.__check_win()
            return True
        return False

    def __check_win(self):
        col_result = self.__check_win_in_col()
        row_rwsult = self.__check_win_in_row()
        return col_result or row_rwsult

    def __check_win_in_col(self):
        for col in self._board:
            p = None
            sum = 0
            for e in col:
                if p is None:
                    p = e
                if p != e:
                    sum = 0
                sum += e
                p = e
                if sum >= 4 or sum <= -4:
                    self._winner = int(sum / abs(sum))
                    return True
        return False

    def __check_win_in_row(self):
        for i in range(len(self._board[0])):
            p = None
            sum = 0
            for j in range(len(self._board)):
                e = self._board[j][i]
                if p is None:
                    p = e
                if e != p:
                    sum = 0
                sum += e
                p = e
                if sum >= 4 or sum <= -4:
                    self._winner = int(sum / abs(sum))
                    return True
        return False

    def __getitem__(self, index):
        return self._board[index]

    def __len__(self):
        return len(self._board)
